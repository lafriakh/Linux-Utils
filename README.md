# Linux & Utils

![Linux & Utils](./Images/main-banner.png)

This is a collection of programs, scripts, notes and personal knowledge about Linux and different applications.

:warning: **This repo is actually for personal use, but feel free to use or contribute, it if you find it useful.** :warning:

### Index

-   <a href="#linux-commands">Linux Commands</a>
-   <a href="#essential-programs">Essential Programs</a>
    -   <a href="#install-essentials">Install Eessentials</a>
    -   <a href="#install-basics">Install Basics</a>
    -   <a href="#install-zsh">Install ZSH</a>
    -   <a href="#install-stars">Install Stars</a>
    -   <a href="#install-design-tools">Install Design Tools</a>
    -   <a href="#install-useful-tools">Install Useful Tools</a>
    -   <a href="#install-customize">Install Customize Tools</a>
    -   <a href="#install-extras">Install Extras</a>
    -   <a href="#install-docker">Install Docker</a>
    -   <a href="#install-node.js">Install Node.js</a>
    -   <a href="#install-python">Install Python</a>
    -   <a href="#install-ruby">Install Ruby</a>
    -   <a href="#install-syncthing">Install Syncthing</a>
-   <a href="#more-basic-programs">More Basic programs</a>
-   <a href="#more-programs">More Programs</a>
-   <a href="#npm-basics">npm Basics</a>
-   <a href="#themes-and-icons">Themes and Icons</a>

--------------------------------------------------------------------------------

## Linux Commands

![Linux Commands](./Images/title-banner.png)

Collection of regularly used commands

<a href="./commands.md" rel="nofollow">
    <img src="./Images/button-green.png" alt="Linux Commands">
</a>

--------------------------------------------------------------------------------

## Essential Programs

![Linux Commands](./Images/title-banner.png)

Collection of scripts to install some essential programs.

### Install Essentials

```bash
$ wget -O xw  https://bit.ly/2yrrJEd && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2yrrJEd)

```bash
# - Software-properties-common
# - Python-software-properties
# - Progress bar
# - Git
# - Curl
# - Vim
# - Htop
# - Tree
# - Ack
```

### Install Basics

```bash
$ wget -O xw  https://bit.ly/2td6ilo && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2td6ilo)

```bash
# - Essential tools for compiling from sources
# - Packing software
# - Zssh (zssh is a program for transferring files to a remote machine )
# - Cloc (Count Lines of Code)
# - Git Cloc
# - Git Extras
```

### Install ZSH

```bash
$ wget -O xw  https://bit.ly/2M8B6Lc && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2M8B6Lc)

```bash
# - Zsh
# - Oh-My-ZSH
# - Insert .zshrc
# - Zsh Syntax highlighting
# - ZSH Git Prompt
# - Awesome Tterminal Fonts
```

### Install Alias

```bash
$ wget -O xw  https://bit.ly/2yucgDm && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2yucgDm)

### Install Stars

```bash
$ wget -O xw  https://bit.ly/2ytMYp8 && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2ytMYp8)

### Install Design Tools

```bash
$ wget -O xw  https://bit.ly/2MFguvb && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2MFguvb)

```bash
# - gPick
# - Shutter
```

### Install Useful Tools

```bash
$ wget -O xw  https://bit.ly/2yusDju && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2yusDju)

```bash
# - Samba
# - Filezilla
# - Mysql Workbench
# - Remmina
# - Cryptkeeper
# - Meld
```

### Install Customize Tools

```bash
$ wget -O xw  https://bit.ly/2lnJNG4 && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2lnJNG4)

```bash
#  - Grub Customizer
```

### Install Extras

```bash
$ wget -O xw  https://bit.ly/2I7JNmr && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2I7JNmr)

```bash
# - Pyrenamer
# - DaemonFSk (Real time file watcher)
# - Gufw
# - Viewnior
# - Nemo Rabbitvcs
```

### Install Docker

```bash
$ wget -O xw  https://bit.ly/2K6XuqM && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2K6XuqM)

### Install Node.js

```bash
$ wget -O xw  https://bit.ly/2I7NmJl && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2I7NmJl)

```bash
# - Nodejs
# - npm
```

### Install Python

```bash
$ wget -O xw  https://bit.ly/2I6Bwz8 && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2I6Bwz8)

### Install Ruby

```bash
$ wget -O xw  https://bit.ly/2JUc3Pd && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2JUc3Pd)

### Install Ruby 2.2.1

```bash
$ wget -O xw  https://bit.ly/2I5QUfa && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2I5QUfa)

### Install Syncthing

```bash
$ wget -O xw  https://bit.ly/2tdW97T && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2tdW97T)

### Fix Locale

```bash
$ wget -O xw  https://bit.ly/2K0xU3j && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2K0xU3j)

### Install zsh no root

```bash
$ wget -O xw  https://bit.ly/2llUBVd && chmod +x xw && ./xw && rm xw
```

[view script raw :page_facing_up:](https://bit.ly/2llUBVd)

--------------------------------------------------------------------------------

**Note:** if you get ERROR: certificate add `--no-check-certificate`

```bash
$ wget --no-check-certificate -O xt  https://bit.ly/2yucgDm && chmod +x xt && ./xt && rm xt
```

--------------------------------------------------------------------------------

## More Basic programs

![Linux Commands](./Images/title-banner.png)

-   [Vagrant](https://www.vagrantup.com/downloads.html)
-   [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
-   [Kitematic](https://github.com/docker/kitematic)
-   [Angry IP Scanner](http://angryip.org/)


```bash
sudo apt install -y alsamixergui
sudo apt install -y gparted
sudo apt install -y hardinfo
sudo apt install -y inkscape
sudo apt install -y uget
```

### Install Yarn

```bash
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update && \
sudo apt-get install -y yarn
```

### Install Ansible

```bash
sudo apt-add-repository -y ppa:ansible/ansible && \
sudo apt-get update && \
sudo apt-get install -y ansible
```
--------------------------------------------------------------------------------

## More programs

![Linux Commands](./Images/title-banner.png)

-   [Exa](https://github.com/ogham/exa)
-   [Fruho VPN Client](https://fruho.com)
-   [Haroopad](http://pad.haroopress.com/)
-   [Koala](http://koala-app.com/)
-   [Krita](https://krita.org)
-   [Nitro Task](http://nitrotasks.com/)
-   [Ocenaudio](http://www.ocenaudio.com)
-   [Pac](http://sourceforge.net/projects/pacmanager/files/)
-   [Pleeease](http://pleeease.io/)
-   [Screamingfrog](https://www.screamingfrog.co.uk/seo-spider/)
-   [Sublime text](http://www.sublimetext.com/3)
-   [Timeshift](http://www.teejeetech.in/p/timeshift.html)
-   [Stacer](https://github.com/oguzhaninan/Stacer)

```bash
sudo apt install -y calibre
sudo apt install -y qshutdown
sudo apt install -y imagemagick
sudo apt install -y luckybackup
sudo apt install -y wine
# Bless Hex Editor
sudo apt install -y bless
# Config Bandwidth
sudo apt install -y trickle
```

### System Info

-   [https://github.com/dylanaraps/neofetch](https://github.com/dylanaraps/neofetch)

```bash
sudo add-apt-repository -y ppa:dawidd0811/neofetch && \
sudo apt update && \
sudo apt install -y neofetch
```

### Install inotify

monitor events [Website](https://github.com/rvoicilas/inotify-tools)

```bash
sudo apt install -y inotify-tools
```

### Install Go For It!

```bash
sudo add-apt-repository -y ppa:mank319/go-for-it && \
sudo apt update && \
sudo apt install -y go-for-it
```

### Peek (Gif)

```bash
sudo add-apt-repository ppa:peek-developers/stable
sudo apt update
sudo apt install peek
```

### Composer, Laravel, Artisan


#### - Install Composer

```bash
sudo apt-get install -y php-cli && \
curl -sS https://getcomposer.org/installer | php && \
sudo mv composer.phar /usr/local/bin/composer

```

```bash
cat >> ${HOME}/.zshrc <<'endmsg'
# | ::::::: Composer ::::::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
export PATH="$HOME/.config/composer/vendor/bin:$PATH"
# | ::::::: Composer ::::::::::::::::::::::::::::::::::::::::::::::::::::::: <<<
endmsg
```


#### - Install Laravel Installer

```bash
composer global require "laravel/installer=~1.1"
```

#### - Create Artisan alias

```bash
cat >> ${HOME}/.zshrc <<'endmsg'
# | ::::::: Artisan php :::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
alias artisan="php artisan"
# | ::::::: Artisan php :::::::::::::::::::::::::::::::::::::::::::::::::::: <<<
endmsg
```

### Qownnotes

```bash
sudo add-apt-repository -y ppa:pbek/qownnotes && \
sudo apt-get update && \
sudo apt-get install -y qownnotes
```

### wkhtmltopdf

Convert html to pdf

```bash
$ sudo apt install -y  wkhtmltopdf

# Use Ex:
$ wkhtmltopdf http://google.com google.pdf
```


### pandoc

Universal document converter

``` bash
$ sudo apt install -y pandoc texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended

## Use Ex:
$
pandoc -f markdown -t html README.md >> README.html
$
pandoc latex.md -o latex.pdf

```

### Pulse Audio equalizer

```bash
sudo add-apt-repository -y ppa:nilarimogard/webupd8 && \
sudo apt-get update && \
sudo apt-get install -y pulseaudio-equalizer
```

### Java 8 or 9

```bash
$
# Java 8
sudo add-apt-repository -y ppa:webupd8team/java && \
sudo apt update && \
sudo apt install -y oracle-java8-installer

# Java 9
sudo add-apt-repository -y ppa:webupd8team/java && \
sudo apt update && \
sudo apt install -y oracle-java9-installer

# Make Java 9 Default
sudo apt-get install oracle-java9-set-default
```


### XBMC

```bash
$
sudo add-apt-repository -y ppa:team-xbmc/ppa && \
sudo apt update && \
sudo apt -y install xbmc
```


### Nasc - Math

Do maths like a normal person. [Website](https://parnold-x.github.io/nasc/)

```bash
$
sudo apt-add-repository -y ppa:nasc-team/daily && \
sudo apt update && \
sudo apt install -y nasc
```

### Brackets [Website](http://brackets.io/)

```bash
sudo add-apt-repository -y ppa:webupd8team/brackets && \
sudo apt-get update && \
sudo apt-get -y install brackets
```


### Salt

```bash
$ sudo apt install -y python-software-properties
$ sudo add-apt-repository -y ppa:saltstack/salt
$ sudo apt update
$ sudo apt install -y salt-master
$ sudo apt install -y salt-minion
$ sudo apt install -y salt-syndic
```

### Docker

```bash
# install docker
curl -sSL https://get.docker.com/ | sudo sh

# add my user to docker group
sudo gpasswd -a ${USER} docker

# restart docker
sudo service docker restart

# tell the current terminal about the new docker group changes
newgrp docker

# Install Docker Compose
sudo apt install python-pip
sudo pip install docker-compose
```


### Linux Brew*

#### - Pre:

```bash
sudo apt install -y \
    build-essential \
    curl \
    git \
    m4 \
    ruby \
    texinfo \
    libbz2-dev \
    libcurl4-openssl-dev \
    libexpat-dev \
    libncurses-dev \
    zlib1g-dev
```

#### - Install:

```bash
git clone https://github.com/Homebrew/linuxbrew.git ~/.linuxbrew
```

#### - Add to .zshrc

```bash
cat >> ${HOME}/.zshrc <<'endmsg'
# | ::::::: Linux Brew ::::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
export PATH="$HOME/.linuxbrew/bin:$PATH"
export MANPATH="$HOME/.linuxbrew/share/man:$MANPATH"
export INFOPATH="$HOME/.linuxbrew/share/info:$INFOPATH"
# | ::::::: Linux Brew ::::::::::::::::::::::::::::::::::::::::::::::::::::: <<<
endmsg
```


### Steam

```bash
wget -c media.steampowered.com/client/installer/steam.deb
sudo dpkg -i steam.deb
sudo apt install -f
```


### Megadown

```bash
$
sudo apt install -y pv
sudo apt install-y php-cli
git clone https://github.com/tonikelope/megadown.git
cd megadown
sudo chmod +x megadown
```


### Megatools

-   [https://github.com/megous/megatools](https://github.com/megous/megatools)

-   [https://packages.debian.org/sid/amd64/megatools/download](https://packages.debian.org/sid/amd64/megatools/download)


### Mysql

```bash
sudo apt install -y \
    mysql-server \
    mysql-client \
    libmysqlclient-dev \
    libmysqld-dev
```


### Postgresql

```bash
sudo apt install -y \
    postgresql \
    postgresql-client \
    postgresql-contrib
```

### Linux Desktop Gnome

``` bash
$
sudo add-apt-repository -y ppa:gnome3-team/gnome3-staging && \
sudo add-apt-repository -y ppa:gnome3-team/gnome3 && \
sudo apt-get update && \
sudo apt-get install -y gnome
```

--------------------------------------------------------------------------------

## MkDocs

-   [Mkdocs](http://www.mkdocs.org/)

-   [Mkdocs Material](https://squidfunk.github.io/mkdocs-material/extensions/admonition/)

#### 01. Pre-Install

```sh
$ sudo apt-get install -y \
                        python-dev \
                        libxml2-dev \
                        libxslt1-dev \
                        antiword \
                        poppler-utils \
                        pstotext
```

```sh
$ sudo easy_install --upgrade google-api-python-client
```
##### pip

```
$ wget --no-check-certificate https://bootstrap.pypa.io/get-pip.py -O - | python - --user

# or

$ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
$ sudo python3 get-pip.py --force-reinstall
$ pip3 install --user --upgrade pip
```

#### 02 Install Mkdocs and Mkdocs Material

```sh
pip3 install --user mkdocs \
                    mkdocs-material \
                    fontawesome_markdown \
                    pymdown-extensions
```


#### Basic Config File

##### Yalm file

Add in ```mkdocs.yml```

+++
```yaml
site_name: Atiica Docs

theme:
  name: 'material'
  logo: 'images/logo.png'
  favicon: 'assets/images/favicon.ico'
  palette:
      primary: 'light-blue'
      accent: 'pink'
  font:
    text: 'Roboto'
    code: 'Roboto Mono'

extra_css:
  - https://use.fontawesome.com/releases/v5.0.6/css/all.css
  - vendors/simpleLightbox/simpleLightbox.min.css
  - ./css/main.min.css

extra_javascript:
  - https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
  - vendors/simpleLightbox/simpleLightbox.min.js
  - ./js/main.min.js

markdown_extensions:
  - codehilite
  - admonition
  - pymdownx.details
  - pymdownx.emoji
```


--------------------------------------------------------------------------------

## npm Basics

![Linux Commands](./Images/title-banner.png)

```bash
npm i -g nativefier
npm i -g localtunnel
npm i -g thanks
npm i -g jshint
npm i -g eslint
npm i -g align-yaml
npm i -g fkill-cli
```

### Update Node.js modules to latest versions

```bash
npm install –g npm-check-updates
```

#### Use:

- Step 1. Check

```bash
ncu
```

- Step 2. Update

```bash
ncu -u
```

- Step 3. Install

```
npm install
```

--------------------------------------------------------------------------------

## Themes and Icons

![Linux Commands](./Images/title-banner.png)

### Cairo-Dock

```bash
sudo add-apt-repository -y ppa:cairo-dock-team/ppa && \
sudo apt update && \
sudo apt-get install -y cairo-dock cairo-dock-plug-ins
```

### Libra

```bash
sudo add-apt-repository -y ppa:noobslab/themes && \
sudo apt update && \
sudo apt install -y libra-theme
```

### vertex

```bash
sudo add-apt-repository -y ppa:noobslab/themes && \
sudo apt update && \
sudo apt install -y vertex-theme
```

### Flattastic

```bash
sudo add-apt-repository ppa:noobslab/themes
sudo apt-get update
sudo apt-get install flattastic-suite
```

### Ambiance ¬ Radiante

```bash
sudo add-apt-repository -y ppa:ravefinity-project/ppa
sudo apt-get update
sudo apt-get install -y radiance-colors ambiance-colors
```

### iOS 7

```bash
sudo add-apt-repository ppa:noobslab/icons
sudo apt update
sudo apt install ieos7-icons
```

### Faience

```bash
# http://tiheum.deviantart.com/art/Faience-icon-theme-255099649
sudo add-apt-repository ppa:tiheum/equinox
sudo apt update
sudo apt install faience-theme faience-icon-theme
```

### Compass Icons

```bash
sudo ppa:noobslab/nitrux-os
sudo apt update
sudo apt install compass-icons
```

### Pacifica Icons

```bash
sudo add-apt-repository ppa:fsvh/pacifica-icon-theme
sudo apt update
sudo apt install pacifica-icon-theme
```

### Nitrux Icons

```bash
sudo add-apt-repository ppa:upubuntu-com/nitrux
sudo apt update
sudo apt install nitruxos
```

### Faience

> /usr/share/icons

``` bash
# http://tiheum.deviantart.com/art/Faience-icon-theme-255099649
sudo add-apt-repository ppa:tiheum/equinox
sudo apt update
sudo apt install faience-theme faience-icon-theme
```

--------------------------------------------------------------------------------

### Image Credits

-   Photo by <a href="https://unsplash.com/@sapegin?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Artem Sapegin">Artem Sapegin</a> on <a href="https://unsplash.com/@sapegin?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Artem Sapegin"> Unsplash </a>
